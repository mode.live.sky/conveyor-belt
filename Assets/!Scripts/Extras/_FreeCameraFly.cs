using UnityEngine;

namespace _CB.Extras
{
    public class _FreeCameraFly : MonoBehaviour
    {
        [SerializeField] private float _movementSpeed = 2f;
        [SerializeField] private float _scale = 2f;

        private float _fastMovementSpeed = 100f;
        private float _freeLookSensitivity = 3f;
        private float _zoomSensitivity = 10f;
        private float _fastZoomSensitivity = 50f;

        private bool _looking = false;


        private void Awake()
        {
            this._fastMovementSpeed = this._movementSpeed * 3f;
            this._freeLookSensitivity = this._movementSpeed / 3f * _scale;
            this._zoomSensitivity = this._movementSpeed * _scale;
            this._fastZoomSensitivity = this._movementSpeed * 5f * _scale;
        }

        private void Update()
        {
            bool fastMode = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            float movementSpeed = fastMode ? this._fastMovementSpeed : this._movementSpeed;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                this.transform.position =
                    this.transform.position +
                    movementSpeed *
                    Time.deltaTime *
                    -this.transform.right;
            }

            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                this.transform.position =
                    this.transform.position +
                    movementSpeed *
                    Time.deltaTime *
                    this.transform.right;
            }

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                this.transform.position =
                    this.transform.position +
                    movementSpeed *
                    Time.deltaTime *
                    this.transform.forward;
            }

            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                this.transform.position = 
                    this.transform.position + 
                    movementSpeed *
                    Time.deltaTime *
                    -this.transform.forward;
            }

            if (Input.GetKey(KeyCode.Q))
            {
                this.transform.position = 
                    this.transform.position + 
                    movementSpeed *
                    Time.deltaTime *
                    this.transform.up;
            }

            if (Input.GetKey(KeyCode.E))
            {
                this.transform.position =
                    this.transform.position +
                    movementSpeed *
                    Time.deltaTime *
                    -this.transform.up;
            }

            if (Input.GetKey(KeyCode.R) || Input.GetKey(KeyCode.PageUp))
            {
                this.transform.position = 
                    this.transform.position + 
                    movementSpeed * Time.deltaTime *
                    Vector3.up;
            }

            if (Input.GetKey(KeyCode.F) || Input.GetKey(KeyCode.PageDown))
            {
                this.transform.position =
                    this.transform.position +
                    movementSpeed *
                    Time.deltaTime *
                    -Vector3.up;
            }

            if (this._looking)
            {
                float newRotationX = this.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * this._freeLookSensitivity;
                float newRotationY = this.transform.localEulerAngles.x - Input.GetAxis("Mouse Y") * this._freeLookSensitivity;
                this.transform.localEulerAngles = new Vector3(newRotationY, newRotationX, 0f);
            }

            float axis = Input.GetAxis("Mouse ScrollWheel");
            if (axis != 0)
            {
                float zoomSensitivity = fastMode ? this._fastZoomSensitivity : this._zoomSensitivity;
                this.transform.position = this.transform.position + this.transform.forward * axis * zoomSensitivity;
            }

            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                _StartLooking();
            }
            else if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                _StopLooking();
            }
        }

        void OnDisable()
        {
            _StopLooking();
        }

        public void _StartLooking()
        {
            this._looking = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        public void _StopLooking()

        {
            this._looking = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}