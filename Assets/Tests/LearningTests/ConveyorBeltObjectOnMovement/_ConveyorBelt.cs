using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Assets.Tests.LearningTests.ConveyorBeltObjectOnMovement
{
    public class _ConveyorBelt : MonoBehaviour
    {
        private readonly string ObjectToTranformTagName = "_TransportObject";

        [SerializeField] private float _conveyorBeltSpeed = 1f;
        [SerializeField] private Vector3 _conveyorBeltMoveDirection = Vector3.zero;

        private List<GameObject> _conveyorBeltTransportGameObjects;


        private void Awake()
        {
            this._conveyorBeltTransportGameObjects = new List<GameObject>();
        }

        private void FixedUpdate()
        {
            if (this._conveyorBeltTransportGameObjects.Count == 0)
            {
                return;
            }

            foreach (GameObject @object in this._conveyorBeltTransportGameObjects)
            {
                if(@object.CompareTag(ObjectToTranformTagName) == false)
                {
                    continue;
                }

                @object.GetComponentInChildren<Rigidbody>().velocity =
                    this._conveyorBeltSpeed * Time.deltaTime * this._conveyorBeltMoveDirection;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            this._conveyorBeltTransportGameObjects.Add(collision.gameObject);
        }

        private void OnCollisionExit(Collision collision)
        {
            this._conveyorBeltTransportGameObjects.Remove(collision.gameObject);
        }
    }
}