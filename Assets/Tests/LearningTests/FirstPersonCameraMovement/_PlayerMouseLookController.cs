using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Tests.LearningTests.FirstPersonCameraMovement
{
    public class _PlayerMouseLookController : MonoBehaviour
    {
        [SerializeField] private float _mouseSensivity = 100f;

        [Space(20)]
        [SerializeField] private Transform _playerBody = default;

        private float _xRotation = default;


        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            mouseX *= this._mouseSensivity * Time.deltaTime;
            mouseY *= this._mouseSensivity * Time.deltaTime;


            this._playerBody.Rotate(mouseX * Vector3.up);


            this._xRotation += -mouseY;
            this._xRotation = Mathf.Clamp(this._xRotation, -90f, 90f);

            this.transform.localRotation = Quaternion.Euler(this._xRotation, 0f, 0f);
        }
    }
}