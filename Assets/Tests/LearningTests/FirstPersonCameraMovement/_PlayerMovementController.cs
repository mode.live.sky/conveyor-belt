using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Tests.LearningTests.FirstPersonCameraMovement
{
    public class _PlayerMovementController : MonoBehaviour
    {
        [SerializeField] private CharacterController _characterController;

        [SerializeField] private float _movementSensivity = 10f;

        private void Update()
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = this.transform.right * x + this.transform.forward * z;

            move *= this._movementSensivity * Time.deltaTime;

            this._characterController.Move(move);

        }
    }
}